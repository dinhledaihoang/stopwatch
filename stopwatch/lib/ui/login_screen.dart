import 'package:flutter/material.dart';
import '../validation/mixin_validation.dart';
//import '../bloc/bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with CommonValidator {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  //final bloc = Bloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login'),),
      body: buildLoginForm(),
    );
  }

  Widget buildLoginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          emailField(),
          passwordField(),
          loginButton()
        ],
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: emailController,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address'
      ),
      validator: validateEmail,
    );
  }

  Widget passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: validatePassword,
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: validate,
        child: Text('Login'));
  }

  // @override
  // void dispose() {
  //   bloc.dispose();
  //   super.dispose();
  // }

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.of(context).pushReplacementNamed('/stopwatch', arguments: email);
    }
  }
}