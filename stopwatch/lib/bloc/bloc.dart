import 'dart:async';

class Bloc {
  final _emailStreamController = StreamController<String>();

  final emailTransformer = StreamTransformer<String, String>.fromHandlers(
      handleData: (String email, sink) {
        if (email.contains('@')) {
          sink.addError('');
        } else {
          sink.addError('$email is an invalid email.');
        }
      },
      handleDone: (sink) => sink.close()
  );

  get changeEmail => _emailStreamController.sink.add;

  Stream<String> get streamEmail => _emailStreamController.stream.transform(emailTransformer);

  dispose() {
    _emailStreamController.close();
  }

}